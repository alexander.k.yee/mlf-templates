# Defaults README

- Place `eisvogel.tex` in the `/home/USR/.config/pandoc/templates/` dir 
(Fedora System).
- `eisvogel.tex` is a tex layout from 
`https://github.com/Wandmalfarbe/pandoc-latex-template`.
- It provides a nice template for the latex reports, hw, and books. Used as the
default for many of my reports.
