# MLF-TEMPLATES

- MLF stands for Markdown-Latex-Format

## Dependencies

- pandoc
- pandoc-citeproc
- markdown
- latex
- make
- bash

## Installation

1. add a new environment variable to your shell's rc file called `MLFREPO`  
```bash
# inside .bashrc or .zshrc or whatever fish uses
export MLFREPO="/path/to/mlf-templates"
```

2. copy script `mlf-templates/src/mk` to your `/usr/local/bin/` dir

## Usage

Note: `mk` is a shortened version of `make`. This is done intentionally
to have a script call that is similar to `make`

### Default
- define a file called `myProject.sources`, where "myProject" can be any name, 
inside your dir full of markdown files
- the `*.sources` needs to be a new line delimited list of your markdown 
files in the order you want them to be rendered for the final pdf report
- inside a dir with `*.md` files run `mk myReport.pdf`


NOTE:
- By default, mk uses IEEE bibliography and in line citation format. 
- The default latex format is the pandoc default.

### Custom Formatting / Pandoc Options

- create a file called `.mkconfig` inside your markdown source dir
- add the option you want to override
    - `export TEMPLATE=/path/to/latex/template`
    - `export CITECSL=/path/to/diff/csl/file`

### Defining templates for `-t` option

- add template file to templates/
- if you want a template that is not general or has personal info add the prefix
"personal" to the template name. E.g. `personalHw`. `.gitignore` will not track these
files
